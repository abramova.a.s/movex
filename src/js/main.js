import $ from 'jquery';
import 'slick-carousel';
import jQueryBridget from 'jquery-bridget';
import Isotope from 'isotope-layout';
jQueryBridget( 'isotope', Isotope, $ );

$(document).ready(function () {
  
  $('.hamburger').click(function(){
    $(this).toggleClass('open');
    $('.nav').toggleClass('active');
  });

  $('.menu-item').click(function() {
    $('.nav').toggleClass('active');
    $('.hamburger').toggleClass('open');
  });

  $('.cover-button__inner').click(function (e) {
    e.preventDefault();
    var aid = $(this).attr("href");
    $('html,body').animate({scrollTop: $(aid).offset().top},'slow');
  });

  var $grid = $('.portfolio').isotope({
    itemSelector: '.portfolio__item',
    layoutMode: 'fitRows'
  });

  var filterFns = {
    // show if number is greater than 50
    numberGreaterThan50: function() {
      var number = $(this).find('.number').text();
      return parseInt( number, 10 ) > 50;
    },
    // show if name ends with -ium
    ium: function() {
      var name = $(this).find('.name').text();
      return name.match( /ium$/ );
    }
  };

  $('.categories-list__item').click(function() {
    $('.categories-list__item--active').removeClass('categories-list__item--active');
    $(this).addClass('categories-list__item--active');
    var filterValue = $( this ).attr('data-filter');
    filterValue = filterFns[ filterValue ] || filterValue;
    $grid.isotope({ filter: filterValue });
  });

  $('.clients').slick({
    lazyLoad: 'ondemand',
    slidesToShow: 4,
    slidesToScroll: 4,
    dots: true,
    arrows: false,
    touchThreshold: 100,
    autoplay: true,
    autoplaySpeed: 2000,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToScroll: 2,
          slidesToShow: 2
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToScroll: 2,
          slidesToShow: 2
        }
      },
    ] 
  });

  $('.process__slider').slick({
    slidesToShow: 2,
    slidesToScroll: 2,
    dots: false,
    arrows: true,
    touchThreshold: 100,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToScroll: 1,
          slidesToShow: 1
        }
      },
    ]
  });

  $('.testimonials').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    touchThreshold: 100,
    autoplay: true,
    autoplaySpeed: 2000,
  });

  $('.about').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: false,
    arrows: false,
    touchThreshold: 100,
    autoplaySpeed: 2000,
    autoplay: true,
    fade: true,
    cssEase: 'linear'
  });
});